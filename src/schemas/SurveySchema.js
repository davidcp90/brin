export const surveyBaseline = [
         {
           name: "Entendamos tu sueño",
           fields: [
             {
               id: "dream_purpose",
               order: 1,
               statement:
                 "¿Cual es el proposito de conseguir una nueva vivienda?",
               instructions: " Selecciona una opción.",
               formType: "single-option",
               options: [
                 {
                   id: "dp_01",
                   statement: "Arrendar para generar más dinero",
                   score: 100
                 },
                 {
                   id: "dp_02",
                   statement: "Construir un hogar junto a mi familia",
                   score: 100
                 },
                 {
                   id: "dp_03",
                   statement: "Mudarme con mi pareja",
                   score: 100
                 },
                 {
                   id: "dp_04",
                   statement: "Independizarme",
                   score: 100
                 },
                 {
                   id: "dp_05",
                   statement:
                     "Vivir alli y sub-arrendar una o más habitaciones",
                   score: 100
                 }
               ]
             },
             {
               id: "dream_location",
               order: 2,
               statement: "¿En que sector te gustaria vivir?",
               instructions:
                 "Posiciona 5 hippos en los barrios en que te gustaria vivir",
               formType: "map-marker",
               permissions: [
                 {
                   type: "geolocation",
                   title: "Activar geolocalización",
                   description:
                     "Ayudanos a entender tus sueños. Enciende la geolocalización para seleccionar los barrios de tus sueños."
                 }
               ]
             },
             {
               id: "dream_price",
               order: 3,
               statement:
                 "¿Cual es el rango de precio en el que esperas comprar tu vivienda?",
               instructions:
                 "Mueve los puntos hasta los valores que mas se ajusten a tu expectativa",
               formType: "range",
               config: {
                 min: 100,
                 max: 1000,
                 start: 260,
                 mid: 540,
                 increment: 20,
                 tooltipFormatter: value => `$${value} Millones`,
                 displayFormatter: value =>
                   `$${value[0]} - ${value[1]} Millones`
               }
             },
             {
               id: "dream_currrentOwner",
               order: 4,
               statement: "¿Actualmente posees una propiedad?",
               instructions:
                 "Mueve los puntos hasta los valores que mas se ajusten a tu expectativa",
               formType: "single-option",
               options: [
                 {
                   id: "dco_1",
                   statement: "Si",
                   score: 100
                 },
                 {
                   id: "dco_2",
                   statement: "No",
                   score: 100
                 }
               ]
             },
             {
               id: "dream_codebtor",
               order: 5,
               statement: "¿Cuentas con un codeudor?",
               formType: "single-option",
               options: [
                 {
                   id: "dc_01",
                   statement: "Si",
                   score: 100
                 },
                 {
                   id: "dc_02",
                   statement: "No",
                   score: 100
                 }
               ]
             }
           ]
         },
         {
           name: "Entendamos tus finanzas",
           fields: [
             {
               id: "finance_savings",
               order: 1,
               statement:
                 "¿Te encuentras ahorrando dinero para conseguir tu hogar?",
               instructions: " Selecciona una opción.",
               formType: "single-option",
               options: [
                 {
                   id: "fs_01",
                   statement: "Empecé el mes pasado.",
                   score: 100
                 },
                 {
                   id: "fs_02",
                   statement: "Estoy ahorrando desde hace algunos meses.",
                   score: 100
                 },
                 {
                   id: "fs_03",
                   statement: "Estoy ahorrando desde hace más de un año.",
                   score: 100
                 },
                 {
                   id: "fs_04",
                   statement: "No, pero quisiera comenzar",
                   score: 100
                 },
                 {
                   id: "fs_05",
                   statement: "No, de pronto cuando me sobre el dinero.",
                   score: 100
                 }
               ]
             },
             {
               id: "finance_savings_percentage",
               order: 2,
               preCondition: [{ finance_savings: ["fs_01", "fs_02", "fs_03"] }],
               statement:
                 "¿Cuál es tu porcentaje de ahorro sobre tus ingresos?",
               instructions: "Ingresa un valor entre 0 y 100%",
               formType: "number"
             },
             {
               id: "finance_savings_possible_percentage",
               order: 2,
               preCondition: [{ finance_savings: ["fs_04", "fs_05"] }],
               statement:
                 "¿Con cuál porcentaje sobre tus ingresos te gustaría empezar a ahorrar?",
               instructions: "Ingresa un valor entre 0 y 100%",
               formType: "number"
             },
             {
               id: "finance_savings_place",
               order: 3,
               preCondition: [{ finance_savings_percentage: true }],
               statement: "¿En dónde estás guardando tus ahorros?",
               instructions: " Selecciona una opción.",
               formType: "single-option",
               options: [
                 {
                   id: "fsp_01",
                   statement: "En una caleta secreta ",
                   score: 100
                 },
                 {
                   id: "fsp_02",
                   statement: "Cuenta AFC",
                   score: 100
                 },
                 {
                   id: "fsp_03",
                   statement: "Cuenta FPV",
                   score: 100
                 },
                 {
                   id: "fsp_04",
                   statement: "Cuenta de ahorros",
                   score: 100
                 },
                 {
                   id: "fsp_05",
                   statement: "Cuenta de fiducia",
                   score: 100
                 },
                 {
                   id: "fsp_06",
                   statement: "Fondo de inversión ",
                   score: 100
                 }
               ]
             },
             {
               id: "finance_income",
               order: 4,
               preCondition: [{ finance_savings: ["fs_01", "fs_02", "fs_03"] }],
               statement: "¿Cuáles son tus ingresos mensuales?",
               instructions: "Ingresa un valor.",
               formType: "number"
             },
             {
               id: "finance_income_partner",
               order: 5,
               preCondition: [{ dream_purpose: ["dp_02", "dp_03", "fs_03"] }],
               statement:
                 "¿Cuáles son los ingresos mensuales de tu pareja/compañer@ de aventura?",
               instructions: "Ingresa un valor(opcional).",
               formType: "number",
               skip: true
             }
           ]
         }
       ];
