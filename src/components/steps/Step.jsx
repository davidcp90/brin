import React from 'react';
import { Typography } from 'antd';
import FormBuilder from '../forms/FormBuilder';

const { Title } = Typography;

const Step = (props) => {
	const {config} = props;
	const {name, fields} = config;
  return (
    <div>
      <Title level={2}>{name}</Title>
      <FormBuilder fields={fields} />
    </div>
  );
}

export default Step;
