import React from "react";
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { Slider, Typography } from 'antd';
import "./RangeFormAtom.css";

const { Title } = Typography;

class RangeFormAtom extends React.Component {
  state = {
    value: [0, 0]
  };

  handleChange = value => {
    this.setState({ value });
  };

  renderDisplay = (value, displayFormatter) => {
    return value[0] > 0 && value[1] > 0 && displayFormatter ? (<Title level={4} className="©">{displayFormatter(value)}</Title>) : null;
  }

  render() {
    const { config } = this.props;
    const { max, min, start, mid, increment, tooltipFormatter, displayFormatter } = config;
    const { value } = this.state;
    const preColor = value >= mid ? "" : "rgba(0, 0, 0, .45)";
    const nextColor = value >= mid ? "rgba(0, 0, 0, .45)" : "";
    return <>
    <div className="range-form-atom">
      <div className="range-form-atom__icon">
        <MinusCircleOutlined style={{ color: preColor }} />
      </div>
      <Slider
        range
        tipFormatter={tooltipFormatter}
        step={increment}
        defaultValue={[start, mid]}
        max={max}
        min={min}
        onChange={this.handleChange}
      />
      <div className="range-form-atom__icon">
        <PlusCircleOutlined style={{ color: nextColor }} />
      </div>
    </div>
    { this.renderDisplay(value, displayFormatter)}
    </>;
  }
}

export default RangeFormAtom;
