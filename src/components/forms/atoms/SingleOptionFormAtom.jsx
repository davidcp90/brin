import { Radio } from "antd";
import * as React from "react";

const radioStyle = {
  display: "block",
  margin: "8px 0",
  height: "auto",
  lineHeight: "18px",
  padding: "8px"
};

const SingleOptionFormAtom = props => {
  const { options } = props;
  return (
    <Radio.Group defaultValue="a" buttonStyle="solid">
      {options.map((o, i) => (
        <Radio.Button key={i} style={radioStyle} value={o.score}>
          {o.statement}
        </Radio.Button>
      ))}
    </Radio.Group>
  );
};

export default SingleOptionFormAtom;
