import { Typography } from "antd";
import * as React from "react";
const { Title, Text } = Typography;
const titleStyle = {
  marginBottom: '32px'
}

const TitleFormAtom = props => {
  const { statement, instructions } = props;
  return (
    <div style={titleStyle}>
      <Title level={3}>{statement}</Title>
      {instructions ? <Text>{instructions}</Text> : null}
    </div>
  );
};

export default TitleFormAtom;
