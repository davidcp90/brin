import * as React from "react";
import { orderBy } from "lodash";
import TitleFormAtom from './atoms/TitleFormAtom';
import RangeFormAtom from "./atoms/RangeFormAtom";
import SingleOptionFormAtom from "./atoms/SingleOptionFormAtom";
import MapMarkerFormAtom from "./atoms/MapMarkerFormAtom";
const fieldStyle= {
  display: "flex",
  flexDirection: "column",
  marginBottom: "32px"
}
const FormBuilder = ({ fields }) => {
  return (
    <>
      {orderBy(fields, "order", "asc").map((field, idx) => {
        const { formType, statement, instructions } = field;
        let renderField;
        if (formType === "single-option") {
          renderField = <SingleOptionFormAtom options={field.options} />;
        } else if (formType === "map-marker") {
          renderField = <MapMarkerFormAtom config={field} />;
        } else if (formType === "range") {
          renderField = <RangeFormAtom config={field.config} />;
        }
        return (
          <div key={idx} style={fieldStyle}>
            <TitleFormAtom statement={statement} instructions={instructions} />
            {renderField}
          </div>
        );
      })}
    </>
  );
};

export default FormBuilder;
