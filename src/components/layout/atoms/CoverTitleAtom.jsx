import React from "react";

import "./CoverTitleAtom.css";



const CoverTitleAtom = props => {
  return <h1 className="cover-title-atom">{props.text}</h1>;
};

export default CoverTitleAtom;
