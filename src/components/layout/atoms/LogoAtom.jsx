import React from "react";
import whLogo from "../../../images/wh-logo.png";

const LogoAtom = props => {
  const logoStyle = {
    width: props.width || "128px",
    marginBottom: "16px",
    height: "auto"
  };
  return <img style={logoStyle} src={whLogo} alt="Logo de Hippo Banco" />;
};

export default LogoAtom;