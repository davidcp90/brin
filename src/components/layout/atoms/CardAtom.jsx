import React from "react";
import { Card } from "antd";
import LogoAtom from "./LogoAtom";
import "./CardAtom.css";
const coverStyle = {
  display: "flex",
  alignItems: "center",
  flexDirection: "column"
};
const CARD_STYLE = {
  centered: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxShadow: "5px 5px 5px -4px rgba(28,106,109,0.8)",
    borderRadius: "4px"
  },
  centeredNoPad: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    boxShadow: "5px 5px 5px -4px rgba(28,106,109,0.8)",
    borderRadius: "4px",
    padding: "0"
  }
};

const CardAtom = props => {
  return (
    <div style={coverStyle}>
      <LogoAtom />
      <Card
        className="card-atom"
        bodyStyle={CARD_STYLE[props.cardStyle]}
        bordered={false}
      >
        {props.children}
      </Card>
    </div>
  );
};

export default CardAtom;