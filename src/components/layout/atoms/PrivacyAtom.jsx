import React from "react";
import { Modal } from "antd";

const PrivacyAtom = props => {
  return (
    <Modal
      title="Politica de tratamiento y protección de datos personales"
      visible={props.visible}
      onOk={props.handleOk}
      okText="Aceptar"
      onCancel={props.handleCancel}
      cancelText="Rechazar"
    >
      <p>
        HippoBanco será el responsable del tratamiento de sus datos y, en tal virtud, podrá recolectar, almacenar, usar informacion personal, financiera, academica, profesional y aquella dispuestas en redes sociales para las siguientes finalidades: 
      </p>
    </Modal>
  );
};

export default PrivacyAtom;
