import React from "react";
import { FacebookFilled, RightCircleFilled } from '@ant-design/icons';
import { Button } from "antd";

const icons = {
  facebook: <FacebookFilled />,
  next: <RightCircleFilled style={{position: "relative", top:"1px"}} />
};

const MainButtonAtom = props => {
  const icon = props.icon ? icons[props.icon] : null;
  const buttonStyle = {
    margin: "32px auto",
    padding: "16px 24px",
    height: "52px",
    textTransform: "uppercase",
    lineHeight: "20px",
    fontWeight: 300,
    border: 0
  };
  if (props.bgColor) {
    buttonStyle["backgroundColor"] = `${props.bgColor}`;
  }
  if (props.margin) {
    buttonStyle["margin"] = `${props.margin}`;
  }
  return (
    <Button type={props.btnType || 'primary'} style={buttonStyle} size="large">
      {icon}
      {props.text}
    </Button>
  );
};

export default MainButtonAtom;
