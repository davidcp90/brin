import React from "react";
import HippoDream from "../../../images/dreams/dreams-cover.svg";
const style = {
  width: "100%",
  height: "auto",
  margin: "16px auto 0 auto",
};
const HippoDreamAtom = () => {
  return (
    <>
      <img src={HippoDream} style={style} alt="un sueño que parece lejano" />
    </>
  );
};

export default HippoDreamAtom;
