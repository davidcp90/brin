import React from "react";
import HippoPig from "../../../images/finances/hippopig.svg";

const style = {
  width: "60%",
  height: "auto",
  margin: "16px auto 0 auto",
};
const HippoPigAtom = () => {
  return (
    <>
      <img src={HippoPig} style={style} alt="tu dinero y esfuerzos son valiosos" />
    </>
  );
};

export default HippoPigAtom;
