import React from "react";
import HippoExp from "../../../images/experience/experience.png";
const style = {
  width: "100%",
  height: "auto",
  margin: "16px auto 0 auto",
};
const HippoExpAtom = () => {
  return (
    <>
      <img src={HippoExp} style={style} alt="Tu experiencia es importante" />
    </>
  );
};

export default HippoExpAtom;
