import React from "react";
import HippoHouse from "../../../images/cover/house.svg";
/*Abs style
const houseStyle = {
  width: "66.6911%",
  height: "auto",
  position: "absolute",
  bottom: 0,
  left: 0,
  zIndex: 0
}*/
const houseStyle = {
  width: "50%",
  height: "auto",
  margin: "24px auto 0 auto",
  position: "relative",
  top: "24px"
};
const HouseAtom = () => {
  return (
      <>
        <img src={HippoHouse} style={houseStyle} alt="la casa de tus sueños" />
      </>
  );
};

export default HouseAtom;