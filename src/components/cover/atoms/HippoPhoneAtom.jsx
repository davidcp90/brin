import React from "react";
import HippoPhone from "../../../images/environment/hippophone.png";

const style = {
  width: "50%",
  height: "auto",
  margin: "16px auto 0 auto",
};
const HippoPhoneAtom = () => {
  return (
    <>
      <img src={HippoPhone} style={style} alt="quien eres importa" />
    </>
  );
};

export default HippoPhoneAtom;
