import React, { useState } from "react";
import { motion } from "framer-motion";
import { Row, Typography, Checkbox, Button } from "antd";
import CardAtom from "../../components/layout/atoms/CardAtom";
import PrivacyAtom from "../../components/layout/atoms/PrivacyAtom";
import CoverTitleAtom from "../../components/layout/atoms/CoverTitleAtom";
import MainButtonAtom from "../../components/layout/atoms/MainButtonAtom";
import HouseAtom from "../../components/cover/atoms/HouseAtom";

const { Text } = Typography;
const variants = {
  visibleHouse: { opacity: 1, transition: { delay: 0.5, duration: 0.5 } },
  visibleTitle: { opacity: 1, transition: { duration: 2 } },
  hidden: { opacity: 0 }
};

const Cover = () => {
  const [showTac, setShowTac] = useState(false);
  const acceptPrivacyTerms = () => {
    setShowTac(false);
  }
  return (
    <>
    <CardAtom padding="0" cardStyle="centered">
      <motion.div initial="hidden" animate="visibleTitle" variants={variants}>
        <Row
          style={{ textAlign: "center", flexDirection: "column" }}
          type="flex"
          justify="center"
          align="middle"
        >
          <CoverTitleAtom text="Cumple tus sueños" />
          <Text type="primary">
            Descubre que necesitas para tener tu casa propia
          </Text>
          <Checkbox style={{ margin: "40px 0 16px 0" }}>
            Acepto politica de tratamiento de datos
          </Checkbox>
          <Button onClick={()=>setShowTac(true)} type="link" style={{textDecoration: "underline", fontSize: "14px"}}>Leer politica de tratamiento de datos</Button>
          <MainButtonAtom
            text="Entrar con Facebook"
            bgColor="#3377f2"
            icon="facebook"
          />
        </Row>
      </motion.div>
      <motion.div initial="hidden" animate="visibleHouse" variants={variants}>
        <Row type="flex" justify="center" align="top">
          <HouseAtom />
        </Row>
      </motion.div>
    </CardAtom>
    <PrivacyAtom visible={showTac} handleOk={acceptPrivacyTerms} handleCancel={() => setShowTac(false)} />
    </>
  );
};

export default Cover;
