import React from "react";
import { motion } from "framer-motion";
import { Row, Typography } from "antd";
import CardAtom from "../../components/layout/atoms/CardAtom";
import CoverTitleAtom from "../../components/layout/atoms/CoverTitleAtom";
import MainButtonAtom from "../../components/layout/atoms/MainButtonAtom";
import HippoExpAtom from "../../components/cover/atoms/HippoExpAtom";

const { Text } = Typography;
const variants = {
  visibleHouse: { opacity: 1, transition: { delay: 0.5, duration: 0.5 } },
  visibleTitle: { opacity: 1, transition: { duration: 2 } },
  hidden: { opacity: 0 }
};

const Experience = () => {
  return (
    <CardAtom padding="0" cardStyle="centeredNoPad">
      <motion.div initial="hidden" animate="visibleTitle" variants={variants}>
        <Row
          style={{
            textAlign: "center",
            flexDirection: "column",
            padding: "24px"
          }}
          type="flex"
          justify="center"
          align="middle"
        >
          <CoverTitleAtom text="Tu experiencia cuenta" />
          <Text type="primary">
            ¿Alguno vez haz sentido que llevas toda tu vida intentando salir adelante, pero <b>el sistema parece estar en tu contra?</b><br/><br/>Nosotros tambien lo hemos experimentado. Por eso <b>quien eres, y que has logrado</b> es importante para nosotros.
          </Text>
          <MainButtonAtom
            margin={"32px auto 8px auto"}
            bgColor="#13007b"
            text="Continuar"
            icon="next"
          />
        </Row>
      </motion.div>
      <motion.div initial="hidden" animate="visibleHouse" variants={variants}>
        <Row type="flex" justify="center" align="top">
          <HippoExpAtom />
        </Row>
      </motion.div>
    </CardAtom>
  );
};

export default Experience;
