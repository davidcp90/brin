import React from "react";
import { motion } from "framer-motion";
import { Row, Typography } from "antd";
import CardAtom from "../../components/layout/atoms/CardAtom";
import CoverTitleAtom from "../../components/layout/atoms/CoverTitleAtom";
import MainButtonAtom from "../../components/layout/atoms/MainButtonAtom";
import HippoPhoneAtom from "../../components/cover/atoms/HippoPhoneAtom";

const { Text } = Typography;
const variants = {
  visibleHouse: { opacity: 1, transition: { delay: 0.5, duration: 0.5 } },
  visibleTitle: { opacity: 1, transition: { duration: 2 } },
  hidden: { opacity: 0 }
};

const Personal = () => {
  return (
    <CardAtom padding="0" cardStyle="centeredNoPad">
      <motion.div initial="hidden" animate="visibleTitle" variants={variants}>
        <Row
          style={{
            textAlign: "center",
            flexDirection: "column",
            padding: "24px"
          }}
          type="flex"
          justify="center"
          align="middle"
        >
          <CoverTitleAtom text="Examinemos tu entorno" />
          <Text type="primary">
            Uno de los factores que hace más inaccesible tener un credito en Colombia, es que los bancos e instituciones financiarias solo tienen en cuenta tu historia crediticia.<br/><br/> En HippoBanco queremos entenderte a ti, y lo que te rodea, para asi poder <b>evaluarte mejor</b>, &nbsp; y a muchos colombianos que en este momento no tienen la posibilidad de acceder a un credito hipotecario.
          </Text>
          <MainButtonAtom
            margin={"32px auto 8px auto"}
            bgColor="#13007b"
            text="Continuar"
            icon="next"
          />
        </Row>
      </motion.div>
      <motion.div initial="hidden" animate="visibleHouse" variants={variants}>
        <Row type="flex" justify="center" align="top">
          <HippoPhoneAtom />
        </Row>
      </motion.div>
    </CardAtom>
  );
};

export default Personal;
