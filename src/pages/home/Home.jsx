import React, { useState } from "react";
import useInterval from "../../utils/UseInterval.Hook";
import "./Home.css";
import LogoAtom from "../../components/layout/atoms/LogoAtom";

import { Typography } from "antd";
const { Title } = Typography;

const TEASER_LIST = [
  "Comprar una casa es imposible.",
  "¿Cómo voy a conseguir una cuota inicial para mi vivienda?",
  "Los bancos son lentos e injustos.",
  "No tengo historia crediticia"
];

const Home = () => {
  const [teaserPosition, setTeaserPosition] = useState(0);
  useInterval(() => {
    const newPosition =
      isNaN(teaserPosition) || teaserPosition >= TEASER_LIST.length - 1
        ? 0
        : teaserPosition + 1;
    setTeaserPosition(newPosition);
  }, 4000);
  return (
    <div className="home">
      <Title className="home__title">
        <LogoAtom width="256px" />
      </Title>
      <Title level={2} type="secondary">
        Alguna vez has pensado...
      </Title>
      <div className="home__teaser-container">
        <Title level={3} className="home__teaser">
          {TEASER_LIST[teaserPosition]}
        </Title>
      </div>
      <Title level={2} type="secondary">
        Nosotros también...
      </Title>
      <Title level={4} type="secondary">
        ... y creemos que podemos ayudarte a cumplir tu sueño de tener casa
        propia.
        <br />
        <br /> Escríbenos a{" "}
        <a style={{color: "white"}} href="mailto:info@hippobanco.com">mihogar@hippobanco.com</a>.
      </Title>
    </div>
  );
};

export default Home;
