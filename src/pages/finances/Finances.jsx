import React from "react";
import { motion } from "framer-motion";
import { Row, Typography } from "antd";
import CardAtom from "../../components/layout/atoms/CardAtom";
import CoverTitleAtom from "../../components/layout/atoms/CoverTitleAtom";
import MainButtonAtom from "../../components/layout/atoms/MainButtonAtom";
import HippoPigAtom from "../../components/cover/atoms/HippoPigAtom";

const { Text } = Typography;
const variants = {
  visibleHouse: { opacity: 1, transition: { delay: 0.5, duration: 0.5 } },
  visibleTitle: { opacity: 1, transition: { duration: 2 } },
  hidden: { opacity: 0 }
};

const Finances = () => {
  return (
    <CardAtom padding="0" cardStyle="centeredNoPad">
      <motion.div initial="hidden" animate="visibleTitle" variants={variants}>
        <Row
          style={{
            textAlign: "center",
            flexDirection: "column",
            padding: "24px"
          }}
          type="flex"
          justify="center"
          align="middle"
        >
          <CoverTitleAtom text="Analicemos tus finanzas" />
          <Text type="primary">
            En esta sección entenderemos tus finanzas, examinaremos que tan bien posicionado te encuentras frente al sistema financiero, y como podrias estarlo con HippoBanco. <br/><strong>¿Sabias que HippoBanco quiere crear un sistema de evaluación crediticia más rapido, justo y seguro?.</strong> <br/> Seguramente lo lograremos con tu ayuda. 
          </Text>
          <MainButtonAtom
            margin={"32px auto 8px auto"}
            bgColor="#13007b"
            text="Continuar"
            icon="next"
          />
        </Row>
      </motion.div>
      <motion.div initial="hidden" animate="visibleHouse" variants={variants}>
        <Row type="flex" justify="center" align="top">
          <HippoPigAtom />
        </Row>
      </motion.div>
    </CardAtom>
  );
};

export default Finances;
