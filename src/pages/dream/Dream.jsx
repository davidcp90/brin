import React from "react";
import { motion } from "framer-motion";
import { Row, Typography } from "antd";
import CardAtom from "../../components/layout/atoms/CardAtom";
import CoverTitleAtom from "../../components/layout/atoms/CoverTitleAtom";
import MainButtonAtom from "../../components/layout/atoms/MainButtonAtom";
import HippoDreamAtom from "../../components/cover/atoms/HippoDreamAtom";

const { Text } = Typography;
const variants = {
  visibleHouse: { opacity: 1, transition: { delay: 0.5, duration: 0.5 } },
  visibleTitle: { opacity: 1, transition: { duration: 2 } },
  hidden: { opacity: 0 }
};

const Dream = () => {
  return (
    <CardAtom padding="0" cardStyle="centeredNoPad">
      <motion.div initial="hidden" animate="visibleTitle" variants={variants}>
        <Row
          style={{
            textAlign: "center",
            flexDirection: "column",
            padding: "24px"
          }}
          type="flex"
          justify="center"
          align="middle"
        >
          <CoverTitleAtom text="Entendamos tu sueño" />
          <Text type="primary">
            A través de las siguientes preguntas entenderemos tu sueño de tener
            casa propia, se muy honest@, y deja volar tu imaginación.
          </Text>
          <MainButtonAtom
            margin={"32px auto 0 auto"}
            bgColor="#13007b"
            text="Comenzar"
            icon="next"
          />
        </Row>
      </motion.div>
      <motion.div initial="hidden" animate="visibleHouse" variants={variants}>
        <Row type="flex" justify="center" align="top">
          <HippoDreamAtom />
        </Row>
      </motion.div>
    </CardAtom>
  );
};

export default Dream;
