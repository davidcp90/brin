import React from 'react';
import './App.css';
import Home from "./pages/home/Home";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Cover from './pages/cover/Cover';
import Dream from "./pages/dream/Dream";
import Finances from "./pages/finances/Finances";
import Personal from "./pages/personal/Personal";
import Experience from "./pages/experience/Experience";
const App = () => {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/tu-casa" component={Cover} />
        <Route exact path="/tu-sueno" component={Dream} />
        <Route exact path="/tus-finanzas" component={Finances} />
        <Route exact path="/tu-entorno" component={Personal} />
        <Route exact path="/tu-experiencia-cuenta" component={Experience} />
      </Router>
    </div>
  );
}

export default App;
