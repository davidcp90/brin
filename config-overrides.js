/* This overrides create-react-app thingies */

const {
    override,
    fixBabelImports,
    addLessLoader,
} = require("customize-cra");


module.exports = override(
  fixBabelImports("babel-plugin-import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      "primary-color": "#06c1d7", //  primary color for all components #18233f
      "link-color": "#81d8d0", // link color
      "success-color": "#81d88d", // success state color
      "warning-color": "#d8d281", // warning state color
      "error-color": "#d881aa", // error state color
      "font-size-base": "18px", // major text font size
      "heading-color": "#13007b", // heading text color
      "text-color": "#17223f", // major text color
      "text-color-secondary": "#ffffff", // secondary text color
      "disabled-color": "#3a3c48b4", // disable state color
      "border-radius-base": "4px", // major border radius
      "border-color-base": "#3a3c48", // major border color
      "box-shadow-base": "0 2px 8px #3a3c48" // major shadow for layers
    },
    ident: "postcss",
    sourceMap: true, // should skip in production
    importLoaders: true,
    localIdentName: "[name]__[local]___[hash:base64:5]"
  })
);