aws s3 sync build/ s3://brin --exclude ".DS_Store/*" --cache-control "max-age=120000" --delete
aws cloudfront create-invalidation \
    --distribution-id EYI58A6KSFSW1 \
    --paths "/*"